const CCLIGHTS = [
	{x: 63, y: 12, w: 3, h: 1, b:5}, // Right edge
	{x: 72, y: 15, w: 1, h: 8, b:5},
	{x: 74, y: 37, w: 1, h: 6, b:5},
	{x: 48, y: 42, w: 8, h: 1, b:5},
	
	{x: 83-63, y: 12, w: 3, h: 1, b:5}, // Left edge
	{x: 83-72, y: 15, w: 1, h: 8, b:5},
	{x: 83-74, y: 37, w: 1, h: 6, b:5},
	{x: 83-48, y: 42, w: 8, h: 1, b:5},
	
	{x: 40, y: 42, w: 3, h: 1, b:5}, // Center
	
	{x: 47, y: 25, w: 9, h: 1, b:5}, // Interior lights
	{x: 64, y: 25, w: 4, h: 1, b:5},
	{x: 68, y: 31, w: 2, h: 1, b:5},
	{x: 72, y: 15, w: 1, h: 8, b:5},
	{x: 61, y: 31, w: 5, h: 1, b:5},
	{x: 50, y: 31, w: 7, h: 1, b:5},
	{x: 48, y: 31, w: 1, h: 1, b:5},
	{x: 46, y: 15, w: 1, h: 8, b:5},
	{x: 46, y: 24, w: 1, h: 2, b:5},
	{x: 45, y: 31, w: 1, h: 1, b:5},
	{x: 46, y: 33, w: 1, h: 6, b:5},
	{x: 36, y: 32, w: 1, h: 3, b:5},
	{x: 36, y: 37, w: 1, h: 3, b:5},
	{x: 36, y: 30, w: 1, h: 1, b:5},
	{x: 34, y: 31, w: 2, h: 1, b:5},
	{x: 24, y: 31, w: 9, h: 1, b:5},
	{x: 16, y: 31, w: 6, h: 1, b:5},
	{x: 14, y: 31, w: 1, h: 1, b:5},
	{x: 15, y: 25, w: 2, h: 1, b:5},
	{x: 23, y: 25, w: 6, h: 1, b:5},
	{x: 32, y: 25, w: 3, h: 1, b:5},
	{x: 36, y: 24, w: 1, h: 1, b:5},
	{x: 36, y: 14, w: 1, h: 2, b:5},
	{x: 36, y: 18, w: 1, h: 2, b:5}
];

const CBLIGHTS = [
	{x: 73, y: 8, w: 15, h: 1, b:5}, // Right lights
	{x: 95, y: 20, w: 1, h: 24, b:5},
	{x: 97, y: 64, w: 1, h: 14, b:5},
	{x: 94, y: 80, w: 1, h: 4, b:5},
	{x: 94, y: 92, w: 1, h: 2, b:5},
	{x: 90, y: 103, w: 2, h: 1, b:5},
	{x: 83, y: 105, w: 6, h: 1, b:5},
	
	{x: 118-73, y: 8, w: 15, h: 1, b:5}, // Left lights
	{x: 118-95, y: 20, w: 1, h: 24, b:5},
	{x: 118-97, y: 64, w: 1, h: 14, b:5},
	{x: 118-94, y: 80, w: 1, h: 4, b:5},
	{x: 118-94, y: 92, w: 1, h: 2, b:5},
	{x: 118-90, y: 103, w: 2, h: 1, b:5},
	{x: 118-83, y: 105, w: 6, h: 1, b:5},
	
	{x: 57, y: 106, w: 4, h: 1, b:5},

	{x: 85, y: 45, w: 6, h: 1, b:5}, // Interior lights
	{x: 81, y: 45, w: 2, h: 1, b:5},
	{x: 71, y: 45, w: 7, h: 1, b:5},
	{x: 67, y: 45, w: 2, h: 1, b:5},
	{x: 63, y: 45, w: 3, h: 1, b:5},
	{x: 54, y: 45, w: 5, h: 1, b:5},
	{x: 47, y: 45, w: 4, h: 1, b:5},
	{x: 43, y: 45, w: 1, h: 1, b:5},
	{x: 38, y: 45, w: 2, h: 1, b:5},
	{x: 29, y: 45, w: 2, h: 1, b:5},
	{x: 26, y: 45, w: 2, h: 1, b:5},
	
	{x: 85, y: 51, w: 5, h: 1, b:5},
	{x: 76, y: 51, w: 4, h: 1, b:5},
	{x: 71, y: 51, w: 3, h: 1, b:5},
	{x: 57, y: 51, w: 5, h: 1, b:5},
	{x: 50, y: 51, w: 3, h: 1, b:5},
	{x: 44, y: 51, w: 3, h: 1, b:5},
	{x: 39, y: 51, w: 3, h: 1, b:5},
	{x: 30, y: 51, w: 4, h: 1, b:5},
	{x: 25, y: 51, w: 3, h: 1, b:5},
	
	{x: 87, y: 85, w: 6, h: 1, b:5},
	{x: 82, y: 85, w: 3, h: 1, b:5},
	{x: 75, y: 85, w: 4, h: 1, b:5},
	{x: 70, y: 85, w: 4, h: 1, b:5},
	{x: 62, y: 85, w: 4, h: 1, b:5},
	{x: 59, y: 85, w: 1, h: 1, b:5},
	{x: 51, y: 85, w: 4, h: 1, b:5},
	{x: 45, y: 85, w: 5, h: 1, b:5},
	{x: 39, y: 85, w: 4, h: 1, b:5},
	{x: 31, y: 85, w: 4, h: 1, b:5},
	{x: 28, y: 85, w: 1, h: 1, b:5},
	
	{x: 90, y: 89, w: 3, h: 1, b:5},
	{x: 85, y: 89, w: 3, h: 1, b:5},
	{x: 82, y: 89, w: 2, h: 1, b:5},
	{x: 76, y: 89, w: 3, h: 1, b:5},
	{x: 71, y: 89, w: 4, h: 1, b:5},
	{x: 68, y: 89, w: 2, h: 1, b:5},
	{x: 64, y: 89, w: 3, h: 1, b:5},
	{x: 59, y: 89, w: 3, h: 1, b:5},
	{x: 55, y: 89, w: 3, h: 1, b:5},
	{x: 47, y: 89, w: 3, h: 1, b:5},
	{x: 43, y: 89, w: 4, h: 1, b:5},
	{x: 35, y: 89, w: 4, h: 1, b:5},
	{x: 25, y: 89, w: 3, h: 1, b:5}
];

const CTLIGHTS = [
	{x: 12, y: 9, w: 15, h: 1, b:5},
	{x: 12, y: 23, w: 4, h: 1, b:5},
	{x: 23, y: 23, w: 4, h: 1, b:5}
]

const CTTLIGHTS = [
	{x: 7, y: 5, w: 10, h: 1, b:5},
	{x: 8, y: 10, w: 8, h: 1, b:5},

	{x: 10, y: 0, w: 4, h: 3, b:10}
]

const CGLIGHTS = [
	{x: 41, y: 7, w: 2, h: 1, b:5}, // Central cabin
	{x: 43, y: 7, w: 2, h: 1, b:5},
	{x: 45, y: 7, w: 2, h: 1, b:5},

	{x: 56, y: 11, w: 1, h: 17, b:5},
	{x: 27, y: 11, w: 1, h: 17, b:5},

	{x: 53, y: 32, w: 1, h: 1, b:5},
	{x: 26, y: 32, w: 1, h: 1, b:5},

	{x: 25, y: 57, w: 1, h: 1, b:5},
	{x: 54, y: 57, w: 1, h: 1, b:5},

	{x: 13, y: 50, w: 6, h: 1, b:5},
	{x: 65, y: 50, w: 6, h: 1, b:5},

	{x: 9, y: 70, w: 9, h: 1, b:5},
	{x: 69, y: 70, w: 9, h: 1, b:5},

	{x: 36, y: 107, w: 1, h: 1, b:5},
	{x: 47, y: 107, w: 1, h: 1, b:5},

	{x: 39, y: 119, w: 1, h: 1, b:5},
	{x: 44, y: 119, w: 1, h: 1, b:5},

	{x: 23, y: 85, w: 1, h: 15, b:5},
	{x: 23, y: 102, w: 1, h: 3, b:5},
	{x: 23, y: 107, w: 1, h: 3, b:5},

	{x: 60, y: 85, w: 1, h: 15, b:5},
	{x: 60, y: 102, w: 1, h: 3, b:5},
	{x: 60, y: 107, w: 1, h: 3, b:5},

	{x: 36, y: 132, w: 3, h: 1, b:5},
	{x: 45, y: 132, w: 3, h: 1, b:5},

	{x: 22, y: 130, w: 3, h: 1, b:5},
	{x: 59, y: 130, w: 3, h: 1, b:5},

	{x: 42, y: 157, w: 1, h: 9, b:5},

	// Engines
	{x: 14, y: 167, w: 6, h: 2, b:10},
	{x: 64, y: 167, w: 6, h: 2, b:10},
	{x: 26, y: 170, w: 8, h: 2, b:10},
	{x: 50, y: 170, w: 8, h: 2, b:10}
	
];

const CC_INTERIOR = [
	"#########################",
	"#####...............#####",
	"#####.......X.......#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#####...............#####",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################"
];

const CB_INTERIOR = [
	"#########################",
	"#########################",
	"######....#####....######",
	"#####...............#####",
	"####.................####",
	"####.................####",
	"#####...............#####",
	"#####...............#####",
	"####.................####",
	"####.................####",
	"#####...............#####",
	"####........X........####",
	"###...###.......###...###",
	"###...###..###..###...###",
	"####..###..###..###..####",
	"###....##..###..##....###",
	"###...................###",
	"####..#############..####",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################",
	"#########################"
];

const C_T_S = new Sprite('./images/cityTurret.png', CTLIGHTS);
const C_TS_S = new Sprite('./images/cityTurretSecondary.png', []);
const C_TT_S = new Sprite('./images/cityTurretTertiary.png', CTTLIGHTS);

// Guards 
const C_G_S = new Sprite('./images/city_guard.png', CGLIGHTS);

class Room {
	constructor(loc, size, parent=null) {
		this.l = loc;
		this.s = size;
		this.p = parent;
		this.c = [];
		this.sdir = null;
		this.sl = null;
	}

	generate(sv, m, iter) { // Create two child rooms, then call generate on them.
		let dir = Math.round(Math.random());
		if (this.s[dir] <= m*2+2) { // Must be able to split into 2 rooms of at least minimum size.
			dir = (dir + 1) % 2;
			if (this.s[dir] <= m*2+2) {
				return;
			}
		}
		// Split
		let smin = Math.max(m, (.5-sv) * this.s[dir]) + 2;
		let smax = Math.min(this.s[dir]-m, (.5+sv) * this.s[dir]) - 1;
		// Generate child rooms
		let sl = Math.round(smin + (smax - smin) * Math.random());
		let cs = this.s.slice();
		cs[dir] = sl;
		let offset = [0,0];
		offset[dir] += sl-1;
		this.c = [new Room(this.l.slice(), cs, this), new Room(add(this.l, offset), sub(this.s, offset), this)];
		this.sdir = dir;
		this.sl = sl;
		// Repeat
		if (iter != 1) {
			this.c.forEach(x=>{
				x.generate(sv, m, iter-1);
			});
		}
	}

	write_room(a) { // Writes this room to an array
		if (this.c.length != 0) {
			this.c.forEach(x=>x.write_room(a));
			// Create a door between rooms
			let doorLoc = [0,0];
			doorLoc[this.sdir] = this.l[this.sdir] + this.sl-1;
			let odir = (this.sdir + 1) % 2;
			// Check if either child has a split that interferes with this.
			let obs = true;
			let offset = [0,0];
			offset[this.sdir] = 1;
			while (obs) {
				obs = false;
				doorLoc[odir] = this.l[odir] + 1 + Math.round((this.s[odir] - 3) * Math.random());
				let check = add(doorLoc, offset);
				if (a[check[0]][check[1]] != '.') {
					obs = true;
				}
				check = sub(doorLoc, offset);
				if (a[check[0]][check[1]] != '.') {
					obs = true;
				}
			}
			//
			a[doorLoc[0]][doorLoc[1]] = '.';

		} else { // Draw a border around this room.
			let s = this.l[0];
			let l1 = this.l[1];
			let l2 = l1 + this.s[1]-1;
			for (let i = 0; i < this.s[0]; i++) {
				a[s+i][l1] = '#'
				a[s+i][l2] = '#'
			}
			//
			s = this.l[1];
			l1 = this.l[0];
			l2 = l1 + this.s[0] - 1;
			for (let i = 0; i < this.s[1]; i++) {
				a[l1][s+i] = '#'
				a[l2][s+i] = '#'
			}
		}
	}

	print() { // Print the top room, for debugging purposes
		let a = [];
		for (let i = 0; i < this.s[0]; i++) {
			let r = [];
			for (let j = 0; j < this.s[1]; j++) {
				r.push('.');
			}
			a.push(r);
		}
		//
		this.write_room(a);
		// Add start and end positions
		let x = Math.round(Math.random());
		let sr = this.c[x];
		let er = this.c[(x+1)%2];
		while (sr.c != 0) {
			sr = rChoice(sr.c);
		}
		while (er.c != 0) {
			er = rChoice(er.c);
		}
		let l = [Math.round((sr.s[0] - 5) * Math.random()) + 2, 
			Math.round((sr.s[1] - 5) * Math.random()) + 2];
		l = add(l, sr.l);
		let startLoc = l;
		a[l[0]][l[1]] = 'S';
		l = [Math.round((er.s[0] - 5) * Math.random()) + 2, 
			Math.round((er.s[1] - 5) * Math.random()) + 2];
		l = add(l, er.l);
		a[l[0]][l[1]] = 'X';
		// Print the array
		for (let i = 0; i < this.s[0]; i++) {
			let r = '';
			for (let j = 0; j < this.s[1]; j++) {
				r += a[i][j];
			}
			console.log(r);
		}
	}

	populate_array() { // Print the top room, for debugging purposes
		let a = [];
		for (let i = 0; i < this.s[0]; i++) {
			let r = [];
			for (let j = 0; j < this.s[1]; j++) {
				r.push('.');
			}
			a.push(r);
		}
		//
		this.write_room(a);
		// Add start and end positions
		let x = Math.round(Math.random());
		let sr = this.c[x];
		let er = this.c[(x+1)%2];
		while (sr.c != 0) {
			sr = rChoice(sr.c);
		}
		while (er.c != 0) {
			er = rChoice(er.c);
		}
		let l = [Math.round((sr.s[0] - 5) * Math.random()) + 2, 
			Math.round((sr.s[1] - 5) * Math.random()) + 2];
		l = add(l, sr.l);
		let startLoc = l;
		a[l[0]][l[1]] = 'S';
		l = [Math.round((er.s[0] - 5) * Math.random()) + 2, 
			Math.round((er.s[1] - 5) * Math.random()) + 2];
		l = add(l, er.l);
		a[l[0]][l[1]] = 'X';
		//
		return [a, startLoc];
	}

}

// Size: The size of the map. 
// Split: The range of a binary split, from the center.
// generate_bsp(25, .3, 3, 2)
function generate_bsp(size, split_value, min_size, iter) {

	let master_room = new Room([0,0], [size,size], 1); // The base room
	master_room.generate(split_value, min_size, iter);
	//master_room.print();
	return master_room.populate_array();
}

class CityGuard {
	constructor(loc, source) {
		this.l = loc.slice();
		this.s = C_G_S;
		//
		this.turnRate = .01;
		this.acc = .1;
		this.max_vel = 1.5;
		//
		this.angle = Math.random() * 2 * Math.PI;
		this.v = pt(this.max_vel, this.angle)
		this.source = source;
		this.eps = source.eps;
		//
		this.turrets = [];
		this.initTurrets();
		//
		this.patrolDist = 1000;
	}
	
	initTurrets() {
		this.turrets = []
		// (loc, base_angle, arc, turnRate, sprite, lightOffset, range, ship)
		// Add the primary turret
		this.turrets.push(
			new Turret([0, 37], Math.PI, 3.7, .01, C_TT_S, 15, 400, this)
		);
	}
	
	hackStart() {
		objs.player.pauseSounds();
		pause(this.source.towerRotateSound);
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
		//
		this.turrets.forEach(x=>{
			x.render(ctx);
		});
	}

	update(objs) {
		let t = this;
		t.awake = t.source.awake;
		let turret_turning = false;
		//
		let mv = mag(t.v);
		if (mv > t.max_vel) {
			t.v = pt(t.max_vel, ang(t.v));
		} else if (!t.thrusting && mv > t.max_vel/2){
			t.v = mul(t.v, .995);
		}
		t.l = add(t.l, t.v);
		//
		if (t.source.awake) {
			// Handle turning
			let turnMod = 0;
			// Get the angle to the current patrol target, or the angle to orbit it if within a certain radius
			let diff = sub(t.target.l, t.l);
			let targetAngle = ang(diff);
			if (mag(diff) < t.patrolDist - 100) {
				targetAngle += Math.PI;
			} else if (mag(diff) < t.patrolDist) {
				targetAngle += Math.PI / 2;
			}
			targetAngle = ang_norm(targetAngle);
			if (targetAngle != -1) {
				t.angle = ang_turn(t.angle, targetAngle, t.turnRate);
				turnMod = 2*ang_dir(targetAngle, t.angle);
				if (t.angle == targetAngle) { targetAngle = -1; }
			}
			// Thrust forwards
			[[14+39, 167+102,6-turnMod],[64+39, 167+102,6+turnMod],[26+39,170+102,8],[50+39,170+102,8]].forEach(m=>{
				let c = 1.5;
				let pl = add(t.l, pt_addAngle([m[0]-80+m[2]/2, 190-m[1]], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
				objs.particles.push(new Particle(pl, pv, m[2]/3));
			});
			t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
			t.thrusting = true;			
		} else {
			t.v = mul(t.v, .995);
		}
		//
		t.hps = [];
		let target = null;
		if (t.source.awake && mag(sub(t.l, objs.player.l)) < 600) {
			target = objs.player;
		}
		t.turrets.forEach(x=>{
			turret_turning = x.update(objs, target) || turret_turning;
			t.hps.push([x.l, x.sprite.radius]);
		});
		return turret_turning;
	}
}

// City Block
class CityBlock {
	constructor(loc, angle, source) {
		this.angle = angle;
		this.l = loc.slice();
		this.s = new Sprite('./images/cityBlock.png', rFilter(CBLIGHTS, .8));
		this.source = source;
		this.entryMessage = "The city block is empty, but sprawling. Directions in ancient languages still glow on the walls.";
		//
		let g = generate_bsp(25, .3, 3, 3)
		//
		this.interior = new Roguelike(this, g[0], []);//new Roguelike(this, CB_INTERIOR, []);
		this.eps = [];
		//this.eps.push(new EntryPoint([59-59, 70-75], [8,15], this, 9));
		this.eps.push(new EntryPoint([59-59, 70-75], g[1], this, 9));
		//
		this.turrets = [];
		this.initTurrets();
		this.awake = true;
	}
	
	initTurrets() {
		this.turrets = []
		// (loc, base_angle, arc, turnRate, sprite, lightOffset, range, ship)
		// Add the primary turret
		this.turrets.push(
			new Turret([0, 37], Math.PI, 3.7, .005, C_T_S, 15, 300, this)
		);
	}
	
	hackStart() {
		objs.player.pauseSounds();
		pause(this.source.towerRotateSound);
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
		//
		t.eps.forEach(e=>{
			e.render(ctx, offset);
		});
		//
		this.turrets.forEach(x=>{
			x.render(ctx);
		});
	}

	update(objs) {
		let t = this;
		let turret_turning = false;
		t.eps.forEach(e=>{
			e.update(objs);
		});
		//
		t.hps = [];
		let target = null;
		if (t.awake && mag(sub(t.l, objs.player.l)) < 600) {
			target = objs.player;
		}
		t.turrets.forEach(x=>{
			turret_turning = x.update(objs, target) || turret_turning;
			t.hps.push([x.l, x.sprite.radius]);
		});
		return turret_turning;
	}
}

// Occasionally change direction
class City {
	constructor(loc) {
		this.s = new Sprite('./images/cityCenter.png', rFilter(CCLIGHTS, .8));
		this.l = loc.slice();

		this.v = [0,0];
		this.angle = Math.random() * 2 * Math.PI;
		this.awake = true;

		this.entryMessage = "At the core of the city is a hub that once coordinated shipping, communication, and defense.";
		this.eps = [];
		this.eps.push(new EntryPoint([28-28, 41-41], [10,15], this, 5));
		//console.log('Generating city center puzzle.');
		//console.log('Done.');
		this.interior = new Roguelike(this, CC_INTERIOR, []);
		//
		this.shieldRadius = 100;
		// City blocks
		this.cityBlocks = [];
		this.cb_count = 3;
		let start_ang = 2*Math.PI/180;
		let cb_dist = 500 + Math.random() * 250;
		let t = this;
		for (let i = 0; i < this.cb_count; i++) {
			start_ang = ang_norm(start_ang + 2*Math.PI/this.cb_count);
			let c = new CityBlock(add(this.l, pt(cb_dist, start_ang)), start_ang, this);
			this.cityBlocks.push(c);
			c.eps.forEach(x=>{
				t.eps.push(x);
			});
		}
		// Guards
		this.guards = [];
		this.guard_count = 1;
		for (let i = 0; i < this.guard_count; i++) {
			let guard = new CityGuard(add(this.l, pt(cb_dist, 2*Math.random()*Math.PI)), this);
			guard.target = rChoice(this.cityBlocks);
			guard.patrolDist = 300;
			this.guards.push(guard);
		}
		this.guard = this.guards[0]; // Make things easier, with one guard.
		//
		this.setLs();
		this.initTurrets();
		//
		this.towerRotateSound = document.createElement("audio");
		this.towerRotateSound.src = './audio/towerrotate.mp3';
		this.shieldSound = document.createElement("audio");
		this.shieldSound.src = './audio/shield.mp3';
	}

	initTurrets() {
		this.turrets = []
		// (loc, base_angle, arc, turnRate, sprite, lightOffset, range, ship)
		// Add the primary turret
		this.turrets.push(
			new Turret([41-41, 27-0], 0, 2.8, .05, C_TS_S, 20, 150, this)
		);
	}

	hackStart() {
		objs.player.pauseSounds();
		pause(this.towerRotateSound);
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		// Draw connections between components
		let shield_count = 0;
		let closestBlock = null;
		let closestDist = 1000;
		this.cityBlocks.forEach(x=>{
			if (t.awake) {
				ctx.shadowBlur = 10;
				ctx.shadowColor = ctx.strokeStyle = x.awake ? '#FFCCAA99' : '#FFFFFF99';
				ctx.lineWidth = x.awake ? Math.random() * 10 : Math.random() * 2;
				ctx.beginPath();
				ctx.moveTo(...l);
				ctx.lineTo(...add(x.l, offset));
				ctx.stroke();
				ctx.shadowBlur = 0;
				ctx.lineWidth = 1;
				if (x.awake) { 
					shield_count += 1; 
					let dist = mag(sub(x.l, objs.player.l));
					if (dist < closestDist) {
						closestDist = dist;
						closestBlock = x;
					}
				}
			}
			//
			x.render(ctx);
		});
		if (closestBlock != null) {
			this.guard.target = closestBlock;
		} else {
			this.guard.target = this;
		}
		// 
		this.guards.forEach(x=>{
			x.render(ctx);
		});
		//
		t.s.draw(ctx, l[0], l[1], t.angle);
		// Draw a circle around the center
		if (shield_count > 0) {
			ctx.shadowBlur = 5;
			ctx.shadowColor = '#FF0000';
			ctx.strokeStyle = '#FFFFFFAA';
			ctx.lineWidth = Math.random() * (5 + shield_count * 2);
			ctx.beginPath();
			ctx.arc(...l,this.shieldRadius * shield_count / t.cb_count, 0, 2*Math.PI)
			ctx.stroke();
			ctx.shadowBlur = 0;
			ctx.lineWidth = 1;
		}
		//
		t.eps.forEach(e=>{
			e.render(ctx, offset);
		});
		//
		this.turrets.forEach(x=>{
			x.render(ctx);
		});
		//
		
	}

	setLs() {
		this.hl = add(pt(-10, this.angle), this.l);
		this.hps = [];
	}

	update(objs) {
		let t = this;
		//
		let turret_turning = false;
		t.setLs();
		//
		t.eps.forEach(e=>{
			e.update(objs);
		});
		//
		t.hps = [];
		let shield_count = 0;
		this.cityBlocks.forEach(x=>{
			turret_turning = x.update(objs) || turret_turning;
			if (x.awake) {
				shield_count += 1;
			}
			x.hps.forEach(x2=>{t.hps.push(x2);});
		});
		// 
		this.guards.forEach(x=>{
			x.update(objs); 
			x.hps.forEach(x2=>{t.hps.push(x2);});
		});
		//
		let target = null;
		if (t.awake && shield_count > 0 && mag(sub(t.l, objs.player.l)) < 600) {
			target = objs.player;
		} 
		t.turrets.forEach(x=>{
			turret_turning = x.update(objs, target) || turret_turning;
			t.hps.push([x.l, x.sprite.radius]);
			x.light.range = target ? x.light.maxRange : 0;
		});
		//
		if (shield_count > 0) {
			t.hps.push([t.l, t.shieldRadius]);
		}
		//
		if (shield_count > 0 && mag(sub(objs.player.l, t.l)) < this.shieldRadius * shield_count / t.cb_count) {
			objs.player.v = pt(mag(objs.player.v), ang(sub(objs.player.l, t.l)));
			play(this.shieldSound);
		}
		//
		if (!objs.hackTower) {
	 		if (turret_turning) {
				play(this.towerRotateSound);
			} else {
				pause(this.towerRotateSound);
			}
		}
	}
}