class Sprite {
	constructor(src, lights, lightColor='#FF0000') {
		let t = this;
		t.ready = false;
		let img = new Image();
		img.src = src;
		t.img = document.createElement("canvas");
		//
		img.onload = function() {
			let ctx = t.img.getContext('2d');
			t.img.width = img.width;
			t.img.height = img.height+5;
			t.radius = mag([img.width/2, img.height/2]);
			ctx.drawImage(img,0,0);
			ctx.fillStyle = '#FFFFFF';
			ctx.shadowColor = lightColor;
			lights.forEach(x=>{
				ctx.shadowBlur = x.b;
				ctx.fillRect(x.x, x.y, x.w, x.h);
			});
			ctx.shadowBlur = 0;
			t.ready = true;
			}
	}
	draw(ctx, x, y, angle) {
		if (this.ready) {
			let img = this.img;
			x = Math.round(x);
			y = Math.round(y);
			ctx.save();
			ctx.translate(x,y);
			ctx.rotate(-angle+Math.PI);
			ctx.drawImage(img, -img.width/2, -img.height/2);
			ctx.restore();
		}
	}
}