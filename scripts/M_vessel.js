const M_Vessel_LIGHTS = [
	{x: 28, y: 5, w: 1, h: 1, b:5},
	{x: 28, y: 9, w: 1, h: 1, b:5},
	{x: 24, y: 48, w: 2, h: 1, b:5},
	{x: 33, y: 48, w: 2, h: 1, b:5},

	{x: 19, y: 33, w: 1, h: 6, b:5},
	{x: 20, y: 39, w: 1, h: 5, b:5},

	{x: 38, y: 33, w: 1, h: 6, b:5},
	{x: 37, y: 39, w: 1, h: 5, b:5},

	{x: 22, y: 52, w: 4, h: 2, b:10},
	{x: 27, y: 52, w: 4, h: 2, b:10},
	{x: 32, y: 52, w: 4, h: 2, b:10},
];
const M_Vessel_SPRITE = new Sprite('./images/M_vessel.png', M_Vessel_LIGHTS);


class M_Vessel {
	constructor(loc) {
		this.s = M_Vessel_SPRITE;
		this.radius = this.s.radius;
		this.l = loc.slice();
		this.v = [0,0];
		this.hps = [[this.l, 20]];
		//
		this.angle = Math.random()*2*Math.PI;
		//
		this.turnRate = .03;
		this.acc = .15;
		this.max_vel = 6.3;
		//
		this.timer = 1000 + Math.random() * 5000;
		//
		this.status = 'standard';
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
	}

	handleHit(objs) {
		this.status = 'flee';
	}

	update(objs) {
		let t = this;
		t.timer -= 1;
		if (t.timer <= 0) {
			t.status = 'flee'; 
		}
		//
		let mv = mag(t.v);
		if (mv > t.max_vel) {
			t.v = pt(t.max_vel, ang(t.v));
		} else if (!t.thrusting && mv > t.max_vel/2){
			t.v = mul(t.v, .995);
		}
		t.l = add(t.l, t.v);
		t.hps[0][0] = t.l;
		//
		let desiredAngle = t.angle;
		let thrust = false;
		let backThrust = false;
		// 
		if (t.status == 'standard') {
			// Check if the player is near the active encounter
			let activeEncounter = objs.towers[objs.towers.length-1];
			if (mag(sub(objs.player.l, activeEncounter.l)) < 700) {
				let ae_diff = sub(t.l, activeEncounter.l);
				if (mag(ae_diff) < 1500) {
					desiredAngle = ang(ae_diff);
				} else if (mag(ae_diff) > 1700) {
					desiredAngle = ang_norm(ang(ae_diff)+Math.PI);
				} else {
					desiredAngle = ang_norm(ang(ae_diff)+Math.PI/2);
				}
				thrust = true;
			} else { 
				let formLoc = add(objs.player.l, pt_addAngle([300,0], objs.player.angle));
				let diff = sub(formLoc, t.l);
				let dist = mag(diff);
				if (dist > 300) { // Move towards player if too far away.
					desiredAngle = ang(diff);
					thrust = true;
				} else if (dist > 200 && ang_dist(ang(diff), objs.player.angle) > Math.PI/2) {
					// If slightly too far from the player and ahead of him, slow down.
					desiredAngle = objs.player.angle;
					thrust = false;
				} else if (dist > 100) {
					desiredAngle = objs.player.angle;
					thrust = true;
				} else if (dist < 75) {
					desiredAngle = objs.player.angle;
					thrust = true;
				}
				let ad = ang_dist(t.angle, desiredAngle);
				if (thrust) { thrust = (ad < Math.PI/2); }
				if (ad > Math.PI/2) { backThrust = true; }
			}
		} else if (t.status = 'flee') {
			thrust = true;
			desiredAngle = ang(sub(t.l, objs.player.l));
		}
		// Handle desired movement patterns
		let ad = ang_dist(t.angle, desiredAngle);
		if (ad > .01) {
			let temp = t.angle;
			t.angle = ang_turn(t.angle, desiredAngle, t.turnRate);
			let c = 1;
			let dir = -1*ang_dir(temp, t.angle);
			let pl = add(t.l, pt_addAngle([dir*3, 15], t.angle));
			let pv = perturb(add(mul(t.v,1), pt(c, t.angle+dir*Math.PI/2)), .5);
			objs.particles.push(new Particle(pl, pv, 2));
			// If turning right, thrust left, and vice versa.
			if (ad > .1) {
				let dir = (ang_dir(t.angle, desiredAngle) == 1) ? -1 : 1;
				let c = 2.5;
				let a = t.angle+dir*Math.PI/2;
				let pl = add(t.l, pt_addAngle([dir*7, -23], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(c, a)), .3);
				objs.particles.push(new Particle(pl, pv, 3));
				//
				t.v = add(t.v, [.5*t.acc*Math.sin(a), .5*t.acc*Math.cos(a)]);
			}
		}
		if (thrust) {
			// Thrust forwards
			let c = 2.5;
			[-1,1].forEach(m=>{
				let x = Math.abs(m);
				let c = 2.5;
				let pl = add(t.l, pt_addAngle([m*5, -28+x*2], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
				objs.particles.push(new Particle(pl, pv, 5-x));
			});
			t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
		}
		if (backThrust) {
			// Thrust backwards
			let c = 2.5;
			[-1,1].forEach(m=>{
				let x = Math.abs(m);
				let c = 3;
				let pl = add(t.l, pt_addAngle([m*15, -10+x*2], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(c, t.angle)), .2);
				objs.particles.push(new Particle(pl, pv, 4));
			});
			t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
		}
	}
}