class EntryPoint {
	constructor(l, ep, s, r=17) {
		this.base_loc = l.slice();
		this.source = s;
		this.update(false);
		this.ep = ep;
		this.delay = 0;
		this.maxDelay = 100;
		this.r=r
	}
	resetDelay() {
		this.delay = this.maxDelay;
	}
	update(objs) {
		this.l = add(this.source.l, pt(mag(this.base_loc), ang(this.base_loc)+this.source.angle));
		if (this.source.awake && objs) {
			let p_dist = mag(sub(this.l, objs.player.l));
			this.delay = Math.max(this.delay-1, 0);
			if (p_dist < this.r && this.delay == 0) {
				objs.hackTower = this.source.interior;
				this.source.hackStart(objs);
				this.source.interior.enter(this.ep);
				this.resetDelay();
				play(beginHackSound);
				addUniqueMessage('You enter the ancient structure. Bringing it offline should allow forward progress.');
				if (this.source.entryMessage) {
					addMessage(this.source.entryMessage);
					this.source.entryMessage = false;
				}
			}
		}
	}
	render(ctx, offset) {
		let l = add(offset, this.l);
		if (this.source.awake) {
			let t = hexadecimal(1 - this.delay / this.maxDelay);
			ctx.strokeStyle = '#999999';
			ctx.beginPath();
			ctx.arc(l[0], l[1], this.r, 0, 2*Math.PI);
			ctx.stroke();
			ctx.strokeStyle = '#FFFFFF' + t;
			ctx.shadowColor = '#FF0000' + t;
			ctx.shadowBlur = 10;
			ctx.beginPath();
			ctx.arc(l[0], l[1], this.r, 0, 2*Math.PI);
			ctx.stroke();
			ctx.beginPath();
			ctx.arc(l[0], l[1], this.r*(1-this.delay/this.maxDelay), 0, 2*Math.PI);
			ctx.stroke();
			ctx.shadowBlur = 0;
		} else {
			ctx.strokeStyle = '#FFFFFF';
			ctx.shadowColor = '#FFFFFF';
			ctx.shadowBlur = 10;
			ctx.beginPath();
			ctx.arc(l[0], l[1], this.r, 0, 2*Math.PI);
			ctx.stroke();
			ctx.shadowBlur = 0;
		}
	}
}