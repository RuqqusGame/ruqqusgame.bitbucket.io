const M_D_LIGHTS = [
	{x: 5, y: 27, w: 1, h: 4, b:5},
	{x: 14, y: 27, w: 1, h: 4, b:5},
];
const M_D_SPRITE = new Sprite('./images/M_drone.png', M_D_LIGHTS);


class M_Drone {
	constructor(loc) {
		this.s = M_D_SPRITE;
		this.radius = this.s.radius;
		this.l = loc.slice();
		this.v = [0,0];
		this.hps = [[this.l, 10]];
		//
		this.angle = Math.random()*2*Math.PI;
		this.nextAngle = -1;
		//
		this.turnRate = .05;
		this.acc = .1;
		this.max_vel = 6.1;
		//
		this.timer = Math.random() * 500;
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
	}

	update(objs) {
		let t = this;
		//
		let mv = mag(t.v);
		if (mv > t.max_vel) {
			t.v = pt(t.max_vel, ang(t.v));
		} else if (!t.thrusting && mv > t.max_vel/2){
			t.v = mul(t.v, .995);
		}
		t.l = add(t.l, t.v);
		t.hps[0][0] = t.l;
		// Thrust forwards
		let c = 2.5;
		let pl = add(t.l, pt_addAngle([0, -19], t.angle));
		let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
		objs.particles.push(new Particle(pl, pv, 3));
		t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
		// If we're turning, turn.
		if (t.nextAngle != -1) {
			//
			let temp = t.angle;
			t.angle = ang_turn(t.angle, t.nextAngle, t.turnRate);
			if (t.angle != t.nextAngle) {
				let c = 1;
				let dir = -1*ang_dir(temp, t.angle);
				let pl = add(t.l, pt_addAngle([dir*1, 10], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(c, t.angle+dir*Math.PI/2)), .5);
				objs.particles.push(new Particle(pl, pv, 1));
			} else {
				t.nextAngle = -1;
			}
		} else {
			// look at the player. If within 70, turn around.
			let diff = sub(objs.player.l, t.l);
			if (mag(diff) < 70) {
				t.nextAngle = ang_norm(ang(diff) + Math.PI);
			}
			// look at every encounter. If within 200, turn around.
			objs.towers.forEach(x=>{
				let diff = sub(x.l, t.l);
				if (mag(diff) < 200) {
					t.nextAngle = ang_norm(ang(diff) + Math.PI);
				}
			});
			// Randomly turn
			if (t.nextAngle == -1 && t.timer <= 0) {
				t.timer = Math.random() * 500;
				t.nextAngle = Math.random() * 2 * Math.PI;
			}
			t.timer -= 1;
		}
	}
}