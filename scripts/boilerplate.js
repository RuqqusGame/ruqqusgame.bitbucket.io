var canvas, screenWidth, screenHeight;
var keysDown = [];
var leftMouseDown = false;
var rightMouseDown = false;
var paused = false;
var mousePos = [0,0]; // Adjusted mouse position; 0,0 is the center of the screen.
const W_KEY = 87;
const A_KEY = 65;
const S_KEY = 83;
const D_KEY = 68;
const UP_KEY = 38;
const LEFT_KEY = 37;
const DOWN_KEY = 40;
const RIGHT_KEY = 39;
const E_KEY = 69;
const SPACE_KEY = 32;
const M_KEY = 77;

const P_KEY = 80;

window.getAnimationFrame =
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
function(callback)
{
	window.setTimeout(callback, 16.6);
};

window.onload = function()
{
	canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d', {alpha: false}); // There's no need for a transparent background.

	window.onresize();

	initControls();

	initGame();

	loop();
};

window.onresize = function() {
	if(!canvas) return;

	screenWidth = canvas.clientWidth;
	screenHeight = canvas.clientHeight;

	canvas.width = screenWidth;
	canvas.height = screenHeight;
};

function keyPress(k) {
	if (objs.hackTower) {
		// Directional keys move the player.
		//rgMove(k);
		objs.hackTower.move(k);
	} else {
		if (k == P_KEY) {
			paused = !paused;
			if (paused) {
				addMessage('Paused.');
			} else {
				addMessage('Unpaused.');
			}			
		}
	}
}

function initControls() {
	window.onkeydown = function(e) {
		if (keysDown.indexOf(e.keyCode) == -1) keysDown.push(e.keyCode);
	};

	window.onkeyup = function(e) {
		var ix = keysDown.indexOf(e.keyCode);
		if ( ix != -1) {
			keysDown.splice(ix, 1);
			keyPress(e.keyCode);
		}
	};

	window.onmousedown = function(e) {
		if (e.button == 0) {
			leftMouseDown = true;
		} else if (e.button == 2) {
			rightMouseDown = true;
		}
	}
	window.onmouseup = function(e) {
		if (e.button == 0) {
			leftMouseDown = false;
		} else if (e.button == 2) {
			rightMouseDown = false;
		}
	}
	window.oncontextmenu = function(e) {
		e.preventDefault();
	}

	window.onmousemove = function(e) {
		mousePos = [e.clientX - screenWidth/2, e.clientY - screenHeight/2];
	}
}