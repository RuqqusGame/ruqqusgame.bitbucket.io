class Shell {
	constructor(l, v, a, s) {
		this.l = l.slice()
		this.v = v.slice()
		this.source = s;
		this.ang = a;
		this.img = document.createElement("canvas");
		this.time = 0;
		//
		let ctx = this.img.getContext('2d');
		this.img.width = 10;
		this.img.height = mag(this.v)*.5 + 10;
		this.radius = mag([this.img.width/2, this.img.height/2]);
		ctx.shadowColor = ctx.strokeStyle = '#FFCCAA';
		ctx.shadowBlur = 10;
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.moveTo(5, 0);
		ctx.lineTo(5, this.img.height);
		ctx.stroke();
	}
	update(objs) {
		this.l = add(this.l, this.v);
		let impact = false;
		let t = this;
		if (objs.player != t.source) {
			let p = objs.player;
			let dist = mag(sub(p.l, t.l));
			if (dist < (p.sprite.radius + t.radius)*3/4) {
				impact = true;
				objs.particles.push(new Particle(t.l, perturb(pt(mag(t.v)/5, ang_norm(t.ang+Math.PI)), .5), t.radius));
			}
		}
		objs.towers.forEach(x=>{
			if (x != t.source) {
				x.hps.forEach(hp=>{
					let hl = hp[0];
					let r = hp[1];
					let dist = mag(sub(hl, t.l));
					if (dist < (r + t.radius)*3/4) {
						impact = true;
						objs.particles.push(new Particle(t.l, perturb(pt(mag(t.v)/5, ang_norm(t.ang+Math.PI)), .5), t.radius));
						//
						addUniqueMessage('The shell explodes upon hitting its mark, but does little in terms of damage.');
					}
				});
			}
		});
		objs.miniEncounters.forEach(x=>{
			if (x != t.source) {
				x.hps.forEach(hp=>{
					let hl = hp[0];
					let r = hp[1];
					let dist = mag(sub(hl, t.l));
					if (dist < (r + t.radius)*3/4) {
						impact = true;
						objs.particles.push(new Particle(t.l, perturb(pt(mag(t.v)/5, ang_norm(t.ang+Math.PI)), .5), t.radius));
						//
						if (x.handleHit) {
							x.handleHit(objs);
						}
						//
						addUniqueMessage('The shell explodes upon hitting its mark, but does little in terms of damage.');
					}
				});
			}
		});
		if (this.time++ == 100 || impact) {
			objs.shells.splice(objs.shells.indexOf(t), 1);
			if (impact) {
				let bulletHitSound = document.createElement("audio");
				bulletHitSound.src = './audio/bullethit.mp3';
				play(bulletHitSound);
			}
		}
	}
	render(ctx) {
		let img = this.img;
 		ctx.save();
 		ctx.translate(...sub(add([screenWidth/2, screenHeight/2], this.l), player.l));
		ctx.rotate(-this.ang+Math.PI);
		ctx.drawImage(img, -img.width/2, -img.height/2);
		ctx.restore();
	}
}