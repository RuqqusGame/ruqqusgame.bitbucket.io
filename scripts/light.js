class Light {
	constructor(source, offset, range) {
		this.source = source;
		this.range = this.maxRange = range;
		this.offset = offset;
		this.update();
	}
	update(objs) {
		this.angle = this.source.angle;
		// Start
		this.start = add(this.source.hl, pt(this.offset, this.source.head_angle));
		this.end = add(this.source.hl, pt(this.offset+this.range, this.source.head_angle));
		//
		if (objs) {
			let p = objs.player;
			let poi = line_circle_poi(p.l, p.sprite.radius, this.source.hl, this.source.head_angle);
			if (poi) {
				let dist = mag(sub(poi, this.source.hl));
				if (dist < this.maxRange && this.range != 0) {
	 				this.source.eps.forEach(e=>{
	 					e.resetDelay();
	 				});
	 				this.range = dist + p.sprite.radius/2;
	 				addUniqueMessage('As the floodlight fixes on your ship, the access points seal temporarily.');
	 			}
			} else {
				this.range = this.maxRange;
			}
		}
	}
	render(ctx, offset) {
		let s = add(this.start, offset);
		let e = add(this.end, offset);
		let grd = ctx.createLinearGradient(s[0], s[1], e[0], e[1]);
		grd.addColorStop(0, '#AAAAAAFF');
		grd.addColorStop(1, '#AAAAAA00');
		//
		let s1 = add(s, pt(this.offset/10, this.source.head_angle+Math.PI/2));
		let s2 = add(s, pt(this.offset/10, this.source.head_angle-Math.PI/2));
		let e1 = add(e, pt(this.range/15, this.source.head_angle+Math.PI/2));
		let e2 = add(e, pt(this.range/15, this.source.head_angle-Math.PI/2));
		//
		ctx.fillStyle = grd;
		ctx.shadowColor = '#FFFFFF';
		ctx.shadowBlur = 10;
		ctx.beginPath();
		ctx.moveTo(...s1);
		ctx.lineTo(...e1);
		ctx.lineTo(...e2);
		ctx.lineTo(...s2);
		ctx.fill();
		ctx.shadowBlur = 0;
	}
}