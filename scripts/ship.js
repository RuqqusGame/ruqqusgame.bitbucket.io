class Ship {
	 	constructor(sprite) {
	 		this.sprite = sprite;
	 		this.l = [0,0]
	 		this.v = [0,0]
	 		this.angle = 0;
	 		this.max_vel = 6;
	 		this.acc = .15;
	 		this.thrusting = false;
	 	}

	 	pauseSounds() {
	 		pause(engineSound);
	 		pause(sound);
	 	}

	 	render(ctx) {
	 		let dl = sub(add(this.l, [screenWidth/2, screenHeight/2]), player.l);
	 		this.sprite.draw(ctx, dl[0], dl[1], this.angle);
	 	}

	 	update(objs) {
	 		let t = this;
	 		let engine = false;
	 		let gunfire = false;
	 		//
	 		let mv = mag(t.v);
	 		if (mv > t.max_vel) {
	 			t.v = pt(t.max_vel, ang(t.v));
	 		} else if (!t.thrusting && mv > t.max_vel/2){
	 			t.v = mul(t.v, .995);
	 		}
	 		t.l = add(t.l, t.v);
	 		//
	 		let temp = t.angle;
	 		let target_angle = ang_norm(Math.atan2(mousePos[0], mousePos[1]));
			t.angle = ang_turn(t.angle, target_angle, .05);
			if (t.angle != temp) {
				let c = 2;
				let dir = -1*ang_dir(temp, t.angle);
				let pl = add(t.l, pt_addAngle([dir*5, 21], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(c, t.angle+dir*Math.PI/2)), .5);
				objs.playerTrail.push(new Particle(pl, pv, 2));
				//
				engine = true;
			}
	 		//
	 		if (keysDown.includes(W_KEY)) {
				//
				let c = 2.5;
				let pl = add(t.l, pt(-27, t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, t.angle)), .1);
				objs.playerTrail.push(new Particle(pl, pv, 5));
				//
				t.v = add(t.v, [t.acc*Math.sin(t.angle), t.acc*Math.cos(t.angle)]);
				t.thrusting = true;
				engine = true;
			} else {
				t.thrusting = false;
			}
			if (keysDown.includes(S_KEY)) {
				[-1,1].forEach(m=>{
					//
					let c = 2;
					let pl = add(t.l, pt_addAngle([m*20, 5], t.angle));
					let pv = perturb(add(mul(t.v,1), pt(c, t.angle)), .3);
					objs.playerTrail.push(new Particle(pl, pv, 3));
				});
				t.v = add(t.v, [.5*t.acc*-Math.sin(t.angle), .5*t.acc*-Math.cos(t.angle)]);
				//
				engine = true;
			}
			//
			if (keysDown.includes(D_KEY)) {
				let c = 2.5;
				let dir = -1;
				let a = t.angle+dir*Math.PI/2;
				let pl = add(t.l, pt_addAngle([dir*-15, -17], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, a)), .3);
				objs.playerTrail.push(new Particle(pl, pv, 3));
				//
				t.v = add(t.v, [.5*t.acc/2*Math.sin(a), .5*t.acc/2*Math.cos(a)]);
				//
				engine = true;
			}
			if (keysDown.includes(A_KEY)) {
				let c = 2.5;
				let dir = 1;
				let a = t.angle+dir*Math.PI/2;
				let pl = add(t.l, pt_addAngle([dir*-15, -17], t.angle));
				let pv = perturb(add(mul(t.v,1), pt(-c, a)), .3);
				objs.playerTrail.push(new Particle(pl, pv, 3));
				//
				t.v = add(t.v, [.5*t.acc/2*Math.sin(a), .5*t.acc/2*Math.cos(a)]);
				//
				engine = true;
			}
			//
			if (leftMouseDown || keysDown.includes(SPACE_KEY)) {
				if (new Date().getTime()%3==1) {
					[-1,1].forEach(m=>{
						//
						let c = 2;
						let pl = add(t.l, pt_addAngle([m*15, 5], t.angle));
						let pv = perturb(add(mul(t.v,1), pt(c, t.angle)), .3);
						objs.particles.push(new Particle(pl, pv, 2));
						//
						pv = perturb(add(mul(t.v,1), pt(c*3, t.angle)), .3);
						objs.shells.push(new Shell(pl,pv, t.angle, t));
						//
						t.v = add(t.v, [-.1*Math.sin(t.angle), -.1*Math.cos(t.angle)]);
					});
				}
				gunfire = true;
			} else {
				gunfire = false;
			}
			//
			if (!objs.hackTower) {
				if (engine) {
					play(engineSound);
				} else {
					pause(engineSound);
				}
				if (gunfire) {
					play(sound);
				} else {
					pause(sound);
				}
			}
	 	}
	 }