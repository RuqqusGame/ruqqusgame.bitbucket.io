const ENCOUNTERS = [Tower, PatrolShip, Fort, Tanker, Destroyer, City];//[City]; //
const TOWER_DIST = 2500;//500; //
SOUND = true;//false; //
var encounter_number = -1;

const MINI_ENCOUNTERS = [M_RadioTower, M_Drone, M_Vessel];
const MINI_ENCOUNTER_DIST = 2000;
const MINI_ENCOUNTER_COUNT = 3;

let background = false;
let objs = {
	particles: [],
	playerTrail: [],
	shells: [],
	towers: [],
	miniEncounters: [],
	player: false,
	hackTower: false,
	messages: [],
	vars: []
};
var currentTower;

const MSGSIZE = 10;
const MSGSPACE = 5;

function addMessage(msg) {
	objs.messages.push([msg, 100]);
	if (objs.messages.length > 10) {
		objs.messages.splice(0,1);
	}
}

function addUniqueMessage(msg) {
	if (!objs.vars[msg]) {
		objs.vars[msg] = true;
		addMessage(msg);
	}
}

function initGame() {
	playerLights = [
		{x: 25, y: 5, w: 1, h: 1, b:2},
		{x: 25, y: 7, w: 1, h: 1, b:2},
		{x: 25, y: 9, w: 1, h: 1, b:2},
		{x: 20, y: 53, w: 10, h: 2, b:10},
	];
	player = new Ship(new Sprite('./images/ship.png', playerLights));
	objs.player = player;
	background = generate_background(2000, 2000, Math.random, false);
	background2 = generate_background(2000, 2000, Math.random, true);
	foreground = generate_background(2000, 2000, Math.random, true);
	//
	addMessage('Welcome to The Embers.');
	addMessage('WASD to thrust, reverse, and strafe. Mouse to rotate.');
}

function renderBackground(ctx, bg, size, loc, scs, parallax=1) {
	// To start with, just render along the X-axis.
	// Draw the background-loc from zero to screen width
	loc = mul(loc.slice(), parallax);
	let xs = loc[0]%size;
	let ys = loc[1]%size;
	let xw = size-loc[0]%size;
	let yw = size-loc[1]%size;
	ctx.drawImage(bg, 
		xs, // Source location
		ys, 
		xw, // Dimensions
		yw, 
		0, // Destination location
		0, 
		xw, // Dimensions
		yw
	);
	//
	if (loc[0] > size-scs[0]) { 
		ctx.drawImage(bg, 0, ys, xs, yw, xw, 0, xs, yw);
	} else if (loc[0] < 0) {
		ctx.drawImage(bg, size+xs, ys, -xs, yw, 0, 0, -xs, yw);
	}
	//
	if (loc[1] > size-scs[1]) { 
		ctx.drawImage(bg, xs, 0, xw, ys, 0, yw, xw, ys);
		//
		if (loc[0] > size-scs[0]) { 
			ctx.drawImage(bg, 0, 0, xs, ys, xw, yw, xs, ys);
		} else if (loc[0] < 0) {
			ctx.drawImage(bg, size+xs, 0, -xs, ys, 0, yw, -xs, ys);
		}
	} else if (loc[1] < 0) {
		ctx.drawImage(bg, xs, size+ys, xw, -ys, 0, 0, xw, -ys);
		//
		if (loc[0] > size-scs[0]) { 
			ctx.drawImage(bg, 0, size+ys, xs, -ys, xw, 0, xs, -ys);
		} else if (loc[0] < 0) {
			ctx.drawImage(bg, size+xs, size+ys, -xs, -ys, 0, 0, -xs, -ys);
		}
	}
}

const img = new Image();
function render() {
	objs.miniEncounters.forEach(x=>{
		x.render(ctx);
	});
	objs.towers.forEach(x=>{
		x.render(ctx);
	});
	// 
	objs.particles.forEach(x=>{
		x.render(ctx);
	});
	// Draw a ship
	player.render(ctx);
	//
	objs.playerTrail.forEach(x=>{
		x.render(ctx);
	});
	// Draw shells
	objs.shells.forEach(x=>{
		x.render(ctx);
	});
	// Draw an arc pointing towards the next encounter
	let tower = objs.towers[objs.towers.length-1];
	if (tower) {
		let diff = sub(tower.l, objs.player.l);
		let magnitude = mag(diff);
		let angle = Math.PI/2-ang(diff);
		if (magnitude > TOWER_DIST) {
			let perc = Math.min(1, (magnitude-TOWER_DIST)/500);
			ctx.strokeStyle = '#FFFFFF' + hexadecimal(perc);
			ctx.beginPath();
			ctx.arc(screenWidth/2, screenHeight/2, objs.player.sprite.radius+10*perc, angle-.25*perc, angle+.25*perc);
			ctx.stroke();
			// Add a line through the arc
			angle = Math.PI / 2 - angle;
			lineStart = add([screenWidth/2, screenHeight/2], pt(objs.player.sprite.radius+5*perc, angle));
			lineEnd = add([screenWidth/2, screenHeight/2], pt(objs.player.sprite.radius+15*perc, angle));
			ctx.beginPath();
			ctx.moveTo(...lineStart);
			ctx.lineTo(...lineEnd);
			ctx.stroke();
			// Add a radar
			ctx.shadowColor = ctx.strokeStyle = '#FFFFFF' + hexadecimal(perc / 2);
			ctx.shadowBlur =  2;
			let radarSize = objs.player.sprite.radius+50*perc;
			ctx.beginPath();
			ctx.arc(screenWidth/2, screenHeight/2, objs.player.sprite.radius+50*perc, 0, 2*Math.PI);
			ctx.stroke();
			objs.towers.forEach(x=>{
				let diff = sub(x.l, objs.player.l);
				if (mag(diff) < TOWER_DIST * 2 && Math.random() > .1) {
					if (x.awake) {
						ctx.shadowColor = ctx.strokeStyle = '#FFCCAA' + hexadecimal(perc / 2);
					} else {
						ctx.shadowColor = ctx.strokeStyle = '#FFFFFF' + hexadecimal(perc / 2);
					}
					let radarLoc = add([screenWidth/2, screenHeight/2], mul(diff, radarSize / (TOWER_DIST*2)));
					ctx.beginPath();
					ctx.arc(...radarLoc, 2, 0, 2*Math.PI);
					ctx.stroke();
				}
			});
			ctx.shadowBlur =  0;
		}
	}
	//
	renderBackground(ctx, foreground, 2000, sub(player.l, [screenWidth/2,screenHeight/2]), [screenWidth, screenHeight], 1.1);
	//
	if (paused) {
		let gradient = ctx.createRadialGradient(screenWidth/2, screenHeight/2, 100, screenWidth/2, screenHeight/2, ((screenWidth/2)**2+(screenHeight/2)**2)**.5);
		gradient.addColorStop(0, '#00000055');
		gradient.addColorStop(1, '#000000FF');
		ctx.fillStyle = gradient;
		ctx.fillRect(0,0,screenWidth,screenHeight);
	}
}

function update() {
	objs.miniEncounters.forEach(x=>{
		x.update(objs);
	});
	objs.towers.forEach(x=>{
		x.update(objs);
	});
	player.update(objs);
	objs.shells.forEach(x=>{
		x.update(objs);
	});
	objs.particles.forEach(x=>{
		x.update(objs.particles);
	});
	objs.playerTrail.forEach(x=>{
		x.update(objs.playerTrail);
	});
}

function getEncounter(loc) {
	if (encounter_number < ENCOUNTERS.length) {
		return new ENCOUNTERS[encounter_number](loc);
	}
	return new ENCOUNTERS[Math.floor(Math.random()*ENCOUNTERS.length)](loc);
}

function getMiniEncounter(loc) {
	return new MINI_ENCOUNTERS[Math.floor(Math.random()*MINI_ENCOUNTERS.length)](loc);
}

function renderMsgs(ctx) {
	let msgX, msgY;
	if (!objs.hackTower) {
		msgX = 25;
		msgY = screenHeight - 50 - objs.messages.length * (MSGSIZE + MSGSPACE);
		//
		let gradient = ctx.createLinearGradient(0, screenHeight, 150, screenHeight-150);
		gradient.addColorStop(0, '#222222FF');
		gradient.addColorStop(1, '#22222200');
		ctx.fillStyle = gradient;
		ctx.fillRect(0, screenHeight-300, 300, 300)
	} else {
		msgX = screenWidth/4;
		msgY = screenHeight*3/4;
		let gradient = ctx.createLinearGradient(0, screenHeight, 0, screenHeight*3/4);
		gradient.addColorStop(0, '#222222FF');
		gradient.addColorStop(1, '#22222200');
		ctx.fillStyle = gradient;
		ctx.fillRect(0, msgY, screenWidth, 300)
	}
	//
	ctx.font = '15px unicode';
	objs.messages.forEach(x=>{
		if (x[1] == 0) {
			ctx.fillStyle = '#888888';
		} else {
			let v = x[1]/100;
			let msgColor = '#' + hexadecimal((136+(119*v))/255) + hexadecimal((136+(68*v))/255) + hexadecimal((136+(32*v))/255)
			x[1] = x[1]-1;
			ctx.fillStyle = msgColor;
			ctx.shadowBlur = 5*v;
		}
		ctx.shadowColor = ctx.fillStyle;
		ctx.shadowBlur = Math.max(ctx.shadowBlur, 1);
		msgY = msgY + MSGSIZE + MSGSPACE;
		ctx.fillText(x[0], msgX, msgY);
		ctx.shadowBlur = 0;
	});
}

function loop() {
	ctx.fillStyle = '#111111';
	ctx.fillRect(0,0,screenWidth,screenHeight);
	renderBackground(ctx, background, 2000, add(player.l, [screenWidth/2,screenHeight/2]), [screenWidth, screenHeight], .5);
	renderBackground(ctx, background2, 2000, add(player.l, [screenWidth/2,screenHeight/2]), [screenWidth, screenHeight], .7);
	// Draw a tower
	render();

	if (objs.hackTower == false && paused == false) {
		update();
		if (!currentTower || currentTower.awake == false) {
			encounter_number += 1;
			let v_ang = ang(player.v);
			// If ang has less than 100 degrees of difference from the line between
			// the player and the center, set ang to loc_ang + 100 + 160*random
			let loc_ang = ang(sub([0,0], player.l));
			if (ang_dist(v_ang, loc_ang) < Math.PI/1.8) {
				v_ang = ang_norm(loc_ang + (Math.PI / 1.8) * (1 + 1.6 * Math.random()));
			}
			//
			let loc = null;
			// Location should be directed away from the center.
			if (!currentTower) {
				loc = add(player.l, perturb(pt(TOWER_DIST, v_ang), 500));
			} else {
				loc = add(currentTower.l, perturb(pt(TOWER_DIST, ang_norm(ang(currentTower.l) + Math.PI * (.5-Math.random()))), 500));
			}
			//
			currentTower = getEncounter(loc);
			objs.towers.push(currentTower);
		}
		// Spawn new mini-encounter
		let ome = objs.miniEncounters;
		if (ome.length == 0 || mag(sub(player.l, ome[ome.length-1].l)) > MINI_ENCOUNTER_DIST*2) {
			let v_ang = ang(player.v);
			let loc = add(player.l, perturb(pt(MINI_ENCOUNTER_DIST, v_ang), 500));
			// Verify that loc isn't anywhere near one of the encounters
			let check = true;
			let count = 0;
			while (check == true && count < 10) {
				check = false;
				count += 1;
				objs.towers.forEach(x => {
					diff = sub(loc, x.l);
					if (mag(x.l, loc) < 500) {
						loc = add(loc, pt(500+Math.random()*200, ang(diff)));
						check = true;
					}
				});
				objs.miniEncounters.forEach(x => {
					diff = sub(loc, x.l);
					if (mag(x.l, loc) < 500) {
						loc = add(loc, pt(500+Math.random()*200, ang(diff)));
						check = true;
					}
				});
			}
			if (count != 10) {
				//
				ome.push(getMiniEncounter(loc));
				if (ome.length > MINI_ENCOUNTER_COUNT) {
					objs.miniEncounters = ome.slice(1)
				}
			}
		}
	} else if (paused == false) {
		objs.hackTower.render(ctx);
	}
	// Render messages
	renderMsgs(ctx);
	//
	getAnimationFrame(loop);
}
