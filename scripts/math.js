function pt(m, a) {
	return [m*Math.sin(a), m*Math.cos(a)];
}
function perturb(p, v) {
	return [p[0]+v*(.5-Math.random()), p[1]+v*(.5-Math.random())];
}
function add(p1, p2) {
	return [p1[0]+p2[0], p1[1]+p2[1]];
}
function sub(p1, p2) {
	return [p1[0]-p2[0], p1[1]-p2[1]];
}
function mul(p, m) {
	return [p[0]*m, p[1]*m];
}
function mag(p) {
	return (p[0]**2+p[1]**2)**.5;
}
function ang(p) {
	return ang_norm(Math.atan2(p[0], p[1]));
}
function pt_addAngle(p, a) {
	return pt(mag(p), ang_norm(a + ang(p)));
}
function ang_norm(a) {
	a = a%(Math.PI*2)
	return (a>=0)?a:a+Math.PI*2;
}
function ang_dist(a1, a2) {
	let d = Math.abs(a1 - a2);
	return (d > Math.PI) ? Math.PI*2-d : d;
}
function ang_dir(a1, a2) {
	let d = a1-a2;
	return (Math.abs(d)>=Math.PI)?Math.sign(d):Math.sign(d)*-1;
}
function ang_turn(a1, a2, r) {
	if (ang_dist(a1, a2) < r) return a2;
	return ang_norm(a1 + ang_dir(a1, a2) * r);
}
function hexadecimal(v) {
	v = Math.round(v*255).toString(16);
	if (v.length == 1) {
		v = '0'+v;
	}
	return v;
}
function rChoice(a) {
	return a[Math.floor(Math.random()*a.length)];
}
function rFilter(a, prob) {
	let na = [];
	a.forEach(x=>{
		if (Math.random() <= prob) {
			na.push(x);
		}
	});
	return na;
}
function quadratic(a, b, c) {
	let t2 = (b**2-4*a*c);
	if (t2 < 0) return false;
	let t3 = 2*a;
	let t1 = -b/t3;
	t2 = (t2**.5)/t3;
	return [t1+t2, t1-t2];
}
function line_circle_poi(cc, cr, lo, la) {
	la = la;
	lo = sub(lo, cc);
	let lm = 1 / Math.tan(la);
	let lb = lo[1]-lo[0]*lm;
	let xs = quadratic((lm**2+1), (2*lm*lb), (lb**2-cr**2));
	if (!xs) return false;
	let xi = (Math.abs(xs[0]-lo[0])<Math.abs(xs[1]-lo[0])) ? 0 : 1;
	let x = xs[xi];
	let y = lm*x+lb;
	let poi = add([x,y], cc);
	lo = add(lo, cc);
	if (ang_dist(la, ang([poi[0]-lo[0], poi[1]-lo[1]])) < .1) return poi;
	x = xs[(xi+1)%2];
	y = lm*x+lb;
	poi = add([x,y], cc)
	if (ang_dist(la, ang([poi[0]-lo[0], poi[1]-lo[1]])) < .1) return poi;
	return false;
}