function generateStars(w,h,d,b,r) {
	let count = Math.round(w*h*d);
	let data = new Uint8ClampedArray(w*h*4);
	for (let i=0; i < count; i++) {
		let l = Math.floor(r()*w*h);
		let c = Math.round(255 * -Math.log(1-r()) * b);
		data[l*4] = c;
		data[l*4+1] = c;
		data[l*4+2] = c;
		data[l*4+3] = 255;
	}
	return data;
}

let img2 = new Image();
img2.src = './images/background.png';
function generate_background(w,h,r, type) {
	let canvas = document.createElement("canvas");
	canvas.width = w;
	canvas.height = h;
	let ctx = canvas.getContext('2d');
	//
	density = 0.025;
	brightness = 0.1
	data = generateStars(w,h,density,brightness,r);
	//
	
	if (type==false) {
		ctx.putImageData(new ImageData(data,w,h),0,0);
	} else {
		ctx.drawImage(img2,0,0);
	}
	return canvas;
}