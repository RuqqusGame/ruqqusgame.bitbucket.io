const M_RT_LIGHTS = [
	{x: 43, y: 1, w: 3, h: 1, b:5},
	{x: 45, y: 15, w: 2, h: 1, b:5},
	{x: 42, y: 29, w: 5, h: 1, b:5},

	{x: 75, y: 1, w: 3, h: 1, b:5},
	{x: 74, y: 15, w: 2, h: 1, b:5},
	{x: 74, y: 29, w: 5, h: 1, b:5},

	{x: 60, y: 28, w: 1, h: 39, b:5},

	{x: 48, y: 112, w: 26, h: 1, b:5},
	{x: 50, y: 119, w: 22, h: 1, b:5},
	{x: 51, y: 126, w: 19, h: 1, b:5}
];

const MRT_LIGHTPAIRS = [[[20-60,72-91], [101-60,72-91]], [[25-60,72-118], [96-60,72-118]]];

class M_RadioTower {
	constructor(loc) {
		let lights = [];
		M_RT_LIGHTS.forEach(x=>{
			if (Math.random() < .8) {
				lights.push(x);
			}
		});
		this.radius = 0;
		this.s = new Sprite('./images/M_radioTower.png', lights);
		this.l = loc.slice();
		this.angle = Math.random()*2*Math.PI;
		this.timer = 0;
		this.hps = [];
	}

	render(ctx) {
		let t = this;
		let offset = sub([screenWidth/2, screenHeight/2], player.l);
		let l = add(t.l, offset);
		t.s.draw(ctx, l[0], l[1], t.angle);
		// Light pair to display
		let lp = t.timer < 30 ? 0 : 1;
		MRT_LIGHTPAIRS[lp].forEach(x=>{
			ctx.beginPath();
			ctx.fillStyle = '#FF0000AA';
			ctx.shadowBlur = 5;
			ctx.arc(...add(t.l, add(pt_addAngle(x, t.angle), offset)), 3, 0, 2*Math.PI);
			ctx.fill();
			ctx.shadowBlur = 0;
		});

	}

	update(objs) {
		this.radius = this.s.radius;
		this.timer = (this.timer + 1) % 60;
	}
}